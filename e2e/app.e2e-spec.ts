import { CiExamplePage } from './app.po';

describe('ci-example App', () => {
  let page: CiExamplePage;

  beforeEach(() => {
    page = new CiExamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
